import React from 'react';
import './assets/css/styles.css';
import { BrowserRouter as Router } from 'react-router-dom';

import Header from './components/Header';
import Slider from './components/Slider';
import Registro from './components/Registro';

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Header />
        </header>
        <section className="componentes">
          <Slider />
          <Registro />
        </section>
      </div>
    </Router>
  );
}


export default App;
