import React from 'react';
import logo from '../assets/images/logo.png';

const Header = () => {


    return (

        <header>
            <div className="logo">
                <img src={logo} width="80px" alt="logo" />
                <h1 className="text-logo">Three nights</h1>
            </div>
            <nav className="nav-items">
                <a href="#popup1"><button type="button" class="nav-item" id="item1"> Registro </button></a>

                <a href="#popup2"><button type="button" class="nav-item" id="item2">iniciar sesión</button></a>
            </nav>
        </header>
    )
};


export default Header;


