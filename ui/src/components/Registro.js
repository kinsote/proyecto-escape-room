import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import '../assets/css/styles_popup.css';

const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
}

const Registro = () => {
    const [name, setName] = useFormField()
    const [birthday, setBirthday] = useFormField()
    const [password, setPassword] = useFormField()
    const [email, setEmail] = useFormField()


    const history = useHistory()
    const [isError, setError] = useState(false)

    const handleSubmit = async (e) => {
        e.preventDefault()
        const user = { name, birthday }
        setError(false)
        try {
            const ret = await fetch('http://localhost:8080/users', {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const data = await ret.json()

            history.push(`/users/${data.id}`)
        } catch (err) {
            console.warn('Error:', err)
            setError(true)
        }
    }

    return (
        <form className="container-popup" id="popup1">

            <div className="popup">

                <div className="container-text">

                    <h1>Registro</h1>

                    <div className="form_login" onSubmit={handleSubmit}>
                        <label>
                            <input name="username" required value={name} onChange={setName} maxlength="20" placeholder="Nombre de usuario" />
                        </label>
                        <label>
                            <input name="birthday" type="date" required value={birthday} onChange={setBirthday} placeholder="Fecha de nacimiento" />
                        </label>
                        <label>
                            <input name="password" type="password" required value={password} onChange={setPassword} placeholder="Contraseña" />
                        </label>
                        <label>
                            <input name="email" type="email" required value={email} onChange={setEmail} placeholder="Correo" />
                        </label>

                        <a href=""><button type="button" class="popup-button" id="item1"> Registrarse </button></a>
                        <a href="#" class="btn-close-popup">X</a>

                    </div>
                </div>
            </div>

            {isError && <div>Error, please try again</div>}
        </form>
    )
}

export default Registro;