import React from 'react';
import img from '../assets/images/fondop1.jpeg'


const Slider = () => {

    return (

        <section className="main">

            <img src={img} alt="fondo-payaso" class="img1" />

            <div className="text-body">
                <p className="item-body">ATREVETE!!!</p>
            </div>

            <div>
                <audio className="audio1" src="/audio/s1.mp3" autoplay loop > </audio>
            </div>

        </section >

    )
}


export default Slider