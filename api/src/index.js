require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const db = require('./db')

const app = express()
app.use(bodyParser.json())
app.use(cors())

// Portada
app.get('/', (req, res) => {
  res.send('API lista para utilizar! ')
})

// API users
app.get('/users', async (req, res, next) => {
  const { rows } = await db.query('SELECT id, name, avatar FROM users')
  res.json(rows)
})

app.get('/users/:id', async (req, res, next) => {
  const { rows } = await db.query(
    'SELECT * FROM users WHERE id = $1',
    [req.params.id]
  )
  if (rows.length) {
    res.json(rows[0])
  } else {
    res.status(404)
    res.json(null)
  }
})

app.post('/users/', async (req, res, next) => {
  try {
    const u = req.body
    const { rows } = await db.query(
      'INSERT INTO users(name, birthday, password, email) ' +
      'VALUES ($1, $2, $3, $4,) RETURNING *',
      [u.name, u.birthday, u.password, u.email]
    )
    res.json(rows[0])
  } catch (e) {
    res.status(400)
    res.json({ error: e.message })
  }
})

app.put('/users/:id', async (req, res, next) => {
  try {
    const u = req.body
    const { rows } = await db.query(
      'UPDATE users SET name = $1, birthday = $2, password = $3, email = $4, id = $5',
      [u.name, u.birthday, u.password, u.email, req.params.id]
    )
    res.json({})
  } catch (e) {
    res.status(400)
    res.json({ error: e.message })
  }
})

// Arrancamos server!
const port = process.env.PORT || 8080
app.listen(port, () => {
  console.log(`API disponible en: http://localhost:${port}`)
})
